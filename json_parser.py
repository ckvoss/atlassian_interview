#! python2.7
# -*- coding: utf-8 -*-

# This solution is fairly easy to read and maintain, however it is not completely optimal and may not scale suitably depending on usage.
# Given the context of this problem, I expect this code would be running client side on a desktop app, mobile app, or web app, in which case this code would be sufficiently performant.
# However, if this code was run server side, there may be performance concerns to address.
# If performance ends up being a concern, there are three approaches I would consider.
# The first solution would be to build a simple scanner and parser (possibly with flex and bison respectively) to parse out the desired data.
# This approach, while a bit more complicated and requiring considerable code, would be easily maintained and expanded upon and would have significant performance gains.
# The second solution I would consider would be to write a function that iterates through the message watching for trigger characters ('@', '(') while keeping the current "word"/token in a buffer to account for URL matching and nested mentions (such as: "hey@chris").
# This approach would be much harder to maintain, very difficult to expand upon, and may not be worth the trouble.
# The third I would consider is to write a map reduce job to process a large batch of message.
# This approach would only make sense if real time results were not required and there was a large volume of data to process.


import json
import re
import urllib2


# TODO:
#  Add proper logging
#  Add sensible exception handling
#  Add more test cases
#  Run pep8 linter (if company best coding practices require)


def jsonify_chat_message(message):
    ret_obj = {}

    # Find Mentions
    mention_regex = re.compile('@\w+')
    mentions = mention_regex.findall(message)
    if mentions:
        ret_obj['mentions'] = []
    for mention in mentions:
        ret_obj['mentions'].append(mention[1:]) # Remove leading @ character

    # Find Emoticons
    emoticon_regex = re.compile('\([a-zA-Z0-9]{1,15}\)')
    emoticons = emoticon_regex.findall(message)
    if emoticons:
        ret_obj['emoticons'] = []
    for emoticon in emoticons:
        ret_obj['emoticons'].append(emoticon[1:-1]) # Remove closing parens

    # Find Links
    link_regex = re.compile('(http|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?') # Sourced from https://stackoverflow.com/questions/6038061/regular-expression-to-find-urls-within-a-string, however there are many many
    links = link_regex.findall(message)
    if links:
        ret_obj['links'] = []
    for link in links:
        link_url = ''.join([link[0], '://',link[1], link[2]]) # TODO: This is sloppy, it should be fixed
        link_obj = {}
        link_obj['url'] = link_url
        # Could use lxml, beautifulsoup, or another doc parser for retrieving the title, but for this case I wanted to stick with the standard library (this executes faster and doesn't rely on external libraries)
        try:
            link_response = urllib2.urlopen(link_url)
            link_html = link_response.read()
            title_regex = re.compile('<title>(.*?)</title>', re.IGNORECASE|re.DOTALL)
            link_title = title_regex.search(link_html).group(1)
            link_obj['title'] = link_title
        except:
            # TODO: Need to figure out likely errors and handle only those, not catch every single error. Also need to find out what the output should be in such a situation (Should the key be omitted or should the value be something specific such as None or an empty string).
            link_obj['title'] = None
        ret_obj['links'].append(link_obj)

    # Prepare and return data
    json_data = json.dumps(ret_obj)
    return json_data


def test_jsonify_chat_message(inputs_and_outputs):
    for supplied_input, expect_output in inputs_and_outputs:
        actual_output = jsonify_chat_message(supplied_input)
        if actual_output != expect_output:
            print "Failure on input: {}".format(supplied_input)
            print "\tActual output:    {}".format(actual_output)
            print "\tExpeceted output: {}".format(expect_output)
        else:
            print "Success on input: {}".format(supplied_input)


# Note I changed up some of the output because the pages titles have changed since this test data was generated. I also added some test cases.
test_data = [("@chris you around?", '{"mentions": ["chris"]}'),
             ("Good morning! (megusta) (coffee)", '{"emoticons": ["megusta", "coffee"]}'),
             ("Olympics are starting soon; http://www.nbcolympics.com", '{"links": [{"url": "http://www.nbcolympics.com", "title": "NBC Olympics | Home of the 2016 Olympic Games in Rio"}]}'),
             ("@bob @john (success) such a cool feature; http://google.com", '{"mentions": ["bob", "john"], "emoticons": ["success"], "links": [{"url": "http://google.com", "title": "Google"}]}'),
             ("https://bitbucket.org", '{"links": [{"url": "https://bitbucket.org", "title": "Bitbucket &mdash; The Git solution for professional teams"}]}'),
             ("Hey @John have you heard about http://www.amazon.com/ or http://www.ebay.com/ ? they're (awesome)!", '{"mentions": ["John"], "emoticons": ["awesome"], "links": [{"url": "http://www.amazon.com/", "title": "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs &amp; more"}, {"url": "http://www.ebay.com/", "title": " Electronics, Cars, Fashion, Collectibles, Coupons and More | eBay "}]}'),
             ]


test_jsonify_chat_message(test_data)
